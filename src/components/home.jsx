import React, { useState } from 'react';
import '../App.scss';
import './styles/home.scss';


const Home = () => {

    const [datos, setDatos] = React.useState({
        nombre: '',
        apellidos: '',
        correo: '',
        telefono: '',
        codigopostal: '',
        colonia: '',
        estadoregion: '',
        ciudad: '',
        municipio: '',
        calle: '',
        direccionSelec: true,
    })


    const [location, setLocaion] = useState({
        status: false,
        city: undefined,
        code: undefined,
        colonies: [],
        state: undefined,
        town: undefined
    })

    const HandleInputChange = (event) => {
        // let nameElement = event.target.name
        console.log(event.target.name)
        console.log(event.target.value)
        // console.log(event.target.name === 'nombre' ? event.target.value : '')


        if (event.target.name === 'codigopostal' || event.target.value === (11000 || 89000)) {
            fetch("https://blackisp.herokuapp.com/postalCodes/" + event.target.value, {
                "method": "GET"
            })
                .then(response => response.json())
                .then(response => {
                    console.log(response)
                    setLocaion({
                        ...location,
                        status: true,
                        city: response.city,
                        code: response.code,
                        colonies: response.colonies,
                        state: response.state,
                        town: response.town
                    });
                    let coloniaField = document.getElementById('colonia').value
                    let cityField = document.getElementById('ciudad').value
                    let municipio = document.getElementById('municipio').value
                    let estate = document.getElementById('estadoregion').value

                    setDatos({
                        ...datos,
                        [event.target.name]: event.target.value,
                        colonia: coloniaField,
                        estadoregion: estate,
                        ciudad: cityField,
                        municipio: municipio
                    })
                })
                .catch(err => {
                    setLocaion({
                        status: false,
                    })
                });
        } else {
            setDatos({
                ...datos,
                [event.target.name]: event.target.value,
            })
        }
        // setDatos({
        //     ...datos,
        //     [event.target.name]: event.target.value
        // })
    }

    // const [newData, setNewData] = useState({
    //     data: []
    // })

    const [message, setMessage] = useState({
        status: false,
        mensaje: ''
    })

    const guardarData = (event) => {
        event.preventDefault();

        let validData = datos.nombre !== '' && datos.apellidos !== '' && datos.correo !== '' && datos.ciudad !== '' && datos.codigopostal !== '' && datos.colonia !== '' && datos.estadoregion !== '' && datos.calle !== '' && datos.direccionSelec !== false;
        console.log(validData)
        console.log(location)
        if (location.status === true) {
            console.log(datos);
            let selfData = [];
            selfData.push(datos)
            if (validData) {
                console.log('data valida');

                fetch('https://blackisp.herokuapp.com/contact', {
                    method: 'POST', // or 'PUT'
                    body: selfData, // data can be `string` or {object}!
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json())
                    .catch(error => { console.error('Error:', error); setMessage({ status: false }) })
                    .then(response => { console.log('Success:', response); setMessage({ status: true }) });
            } else {
                console.log('no data valida')
            }
        } else {
            console.log(datos)
            if (validData) {
                console.log('data valida')
            } else {
                console.log('no data valida')
            }
        }

    }

    const [products, setProducts] = useState(null)

    const [totales, setTotales] = useState(0)

    const getData = async () => await fetch("https://blackisp.herokuapp.com/products", {
        "method": "GET"
    })
        .then(response => response.json())
        .then(data => {
            setProducts(data);
            let price = 0;
            data.forEach(product => {
                let getPrice = parseInt(product.price)
                console.log(getPrice)
                price += getPrice
            });
            setTotales(parseInt(price))
        })
        .catch(err => {
            console.log('error')
        })

    React.useEffect(() => {
        getData()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    console.log(products, totales)

    return (
        <div className="container">
            <div className="row wrap fullheight vertical-center horizontal-center">
                <div className="address-box">
                    <div className="title-blox">
                        <h4>Dirección de envío</h4>
                    </div>

                    <div className="box-content row wrap horizontal-center">
                        <form className="form-box" onSubmit={guardarData}>
                            {message.status === true ?
                                <div className="msj">
                                    Datos enviados con exito
                                </div> : ''
                            }
                            <div className="input-field">
                                <span>
                                    <i className="fas fa-user"></i>
                                </span>
                                <input type="text" required placeholder="Nombre" name="nombre" onChange={HandleInputChange} />
                            </div>
                            <div className="input-field">
                                <span>
                                    <i className="fas fa-user"></i>
                                </span>
                                <input type="text" required placeholder="Apellidos" name="apellidos" onChange={HandleInputChange} />
                            </div>
                            <div className="input-field">
                                <span>
                                    <i className="fas fa-user"></i>
                                </span>
                                <input type="text" required placeholder="Correo Electrónico" name="correo" onChange={HandleInputChange} />
                            </div>
                            <div className="input-field">
                                <span>
                                    <i className="fas fa-user"></i>
                                </span>
                                <input type="text" required placeholder="Número de teléfono" name="telefono" onChange={HandleInputChange} />
                            </div>
                            <div className="input-field">
                                <span>
                                    <i className="fas fa-user"></i>
                                </span>
                                <input type="text" required placeholder="Código postal" name="codigopostal" onChange={HandleInputChange} />
                            </div>

                            <div className="input-field">
                                <span>
                                    <i className="fas fa-user"></i>
                                </span>
                                {location.status === false || location.colonies === undefined ?
                                    <input type="text" required placeholder="Colonia" name="colonia" onChange={HandleInputChange} />
                                    :
                                    <select name="colonia" id="colonia">
                                        {location.colonies.map((colonia, index) => {
                                            return (
                                                <option key={index} value={colonia}>{colonia}</option>
                                            )
                                        })}
                                    </select>
                                }
                            </div>
                            <div className="input-field">
                                <span>
                                    <i className="fas fa-user"></i>
                                </span>
                                <input id="estadoregion" type="text" required placeholder="Estado/Región" defaultValue={location.status === false ? '' : location.state} name="estadoregion" onChange={HandleInputChange} />
                            </div>
                            <div className="input-field">
                                <span>
                                    <i className="fas fa-user"></i>
                                </span>
                                <input id="ciudad" type="text" required placeholder="Ciudad" name="ciudad" defaultValue={location.status === false ? '' : location.city} onChange={HandleInputChange} />
                            </div>
                            <div className="input-field">
                                <span>
                                    <i className="fas fa-user"></i>
                                </span>
                                <input id="municipio" type="text" required placeholder="Delegación o municipio" name="municipio" defaultValue={location.status === false ? '' : location.town} onChange={HandleInputChange} />
                            </div>
                            <div className="input-field">
                                <span>
                                    <i className="fas fa-user"></i>
                                </span>
                                <input type="text" required placeholder="Calle" name="calle" onChange={HandleInputChange} />
                            </div>
                            <div className="actions-box">
                                <button>Libreta de direcciones</button>
                                <button style={{ maxWidth: '100px' }} type="submit">Guardar</button>
                            </div>
                            <div>
                                <input type="checkbox" name="shippingAddress" id="shippingAddress" defaultChecked={datos.direccionSelec} onChange={HandleInputChange} />
                                <label htmlFor="shippingAddress">Utilizar como direccion de facturación.</label>
                            </div>
                        </form>
                    </div>
                </div>
                <div className="shipping-box">
                    <div className="title-product">
                        RESUMEN DE LA ORDEN
                    </div>
                    {products !== null ?
                        <ul>
                            {products.map((product, index) => {
                                return (
                                    <li className="row product" key={index} id={index}>
                                        <img src={product.image} alt={product.name} />
                                        <sub>{product.name}</sub>
                                        <span>{product.price}</span>
                                    </li>
                                )
                            })}
                            <li style={{ display: 'flex', alignItems: 'flex-end', justifyContent: 'flex-end', flexFlow: 'row' }}>
                                <button className="btn-edit">Editar</button>
                            </li>
                            <li className="subtotales">
                                <div><span>SUBTOTAL</span><span>$ {(totales).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&.')}</span></div>
                                <div><span>ENVÍO</span><span>A calcular</span></div>
                            </li>
                            <li className="totales">
                                <div>
                                    <span>TOTAL</span>
                                    <span>$ {(totales).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&.')}</span>
                                </div>
                            </li>
                        </ul> : <div>no hay productos</div>
                    }

                </div>
            </div>
        </div>
    )
}

export default Home;
